/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {  
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo; 
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) { 
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  } 
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) { 
  return $('<div></div>').html('<i>' + sporocilo + '</i>'); 
}


//naloga 3:

function jeSlika(sporocilo, zacetniIndexIskanja){
    
    for(var i = zacetniIndexIskanja; i < sporocilo.length; i++){
      if(sporocilo.substring(i, i+4) == "http"){
        for(var j = 0; j < sporocilo.length; j++){
          if(sporocilo.substring(j, j+4) == ".png" || sporocilo.substring(j, j+4) == ".jpg" || sporocilo.substring(j, j+4) == ".gif"){
            console.log("Najdu slikco");
            var koordinate = [i, j+4];
            return koordinate;
          }  
        }
      }
    }
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    
    
     // Naloga 3: kle notr scene
    
      var scene = [0, 0];

      if(typeof jeSlika(sporocilo, 0) !== 'undefined'){
          scene = jeSlika(sporocilo, 0);
          console.log(scene[0] + " " + scene[1]);
          console.log('<img style="width:200px; padding-left:20px" src="' + sporocilo.substring(scene[0], scene[1]) + '">');
          $('#sporocila').append($('<div></div>').html('<img style="width:200px; padding-left:20px" src="' + sporocilo.substring(scene[0], scene[1]) + '">'));
          }
      
    
  }
  

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  /*//Naloga IV:
  socket.on('preimenovanjeOdgovor', function(rezultat){
    console.log("Hello bitch");
    console.log(rezultat.hello);
  }); */
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    console.log("vzdevek... ja ni mi ratal " + sporocilo.besedilo);
    $('#sporocila').append(novElement);
    
    // Naloga 3: kle notr scene
    
      var scene = [0, 0];

      if(typeof jeSlika(sporocilo.besedilo, 0) !== 'undefined'){
          scene = jeSlika(sporocilo.besedilo, 0);
          console.log(scene[0] + " " + scene[1]);
          console.log('<img style="width:200px; padding-left:20px" src="' + sporocilo.besedilo.substring(scene[0], scene[1]) + '">');
          $('#sporocila').append($('<div></div>').html('<img style="width:200px; padding-left:20px" src="' + sporocilo.besedilo.substring(scene[0], scene[1]) + '">'));
          }

  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  

    
  });

  
  // Naloga IV:
  console.log("preimenovanje" + preimenovanjaKdo[0] + preimenovanjaNaKaj[0]);

  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    var nadimek = 0;
    for (var i=0; i < uporabniki.length; i++) {
      
      for(var j = 0; j < preimenovanjaKdo.length; j++){
        if(preimenovanjaKdo[j] == uporabniki[i]){
          //console.log("preimenovanje" + preimenovanjaKdo[0] + preimenovanjaNaKaj[0]);
          $('#seznam-uporabnikov').append(divElementEnostavniTekst(preimenovanjaNaKaj[j] + " " + uporabniki[i]));
          nadimek = 1;
        }  
      } 
      if(nadimek == 0){
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
      nadimek = 0;
    }
    

     //  Naloga I
   $('#seznam-uporabnikov div').click(function() {
      klepetApp.procesirajUkaz('/zasebno "' + $(this).text() + '" "☜"');
      //$('#posljiSporocilo').val('/zasebno "' + $(this).text() + '"' + &#9756;);
      $('#poslji-sporocilo').focus();
  });
    


  });
  
  
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  

  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
